from typing import Optional

from pydantic import BaseModel


class CarsSchema(BaseModel):
    model: str
    description: str


class DillerSchema(BaseModel):
    name: str
    address: str
    phone_number: int


class DillerCarSchema(BaseModel):
    diller_id: int
    car_id: int
    amount: int
