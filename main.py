from fastapi import Depends, FastAPI, HTTPException
from sqlalchemy.orm import Session
import crud
import models
import schemas
from database import SessionLocal, engine

models.Base.metadata.create_all(bind=engine)

app = FastAPI()


def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()


@app.get("/dillers/{id}")
def read_diller(id: int, db: Session = Depends(get_db)):
    db_diller = crud.get_diller(db, id)
    if db_diller is None:
        raise HTTPException(status_code=404, detail="Diller with this id not found")
    return db_diller


@app.post("/dillers/")
def create_dillers(diller: schemas.DillerSchema, db: Session = Depends(get_db)):
    return crud.create_diller(db=db, diller=diller)


@app.get("/cars/{id}")
def read_car(id: int, db: Session = Depends(get_db)):
    car = crud.get_car(db, id)
    if car is None:
        raise HTTPException(status_code=404, detail="Car with this id not found")
    return car


@app.post("/cars/")
def create_cars(car: schemas.CarsSchema, db: Session = Depends(get_db)):
    return crud.create_car(db=db, car=car)


@app.get("/dillers/")
def read_all_dillers(db: Session = Depends(get_db)):
    return crud.get_all_dillers(db=db)


@app.post("/cars_owners/")
def create_car_own(car_owner: schemas.DillerCarSchema, db: Session = Depends(get_db)):
    car_id = crud.get_car(db, car_owner.car_id)
    diller_id = crud.get_diller(db, car_owner.diller_id)
    if car_id is None:
        raise HTTPException(status_code=404, detail="Car with this id not found")
    if diller_id is None:
        raise HTTPException(status_code=404, detail="Diller with this id not found")

    return crud.create_car_own(db=db, car_diller=car_owner)


@app.get("/dillers/{id}/cars")
def read_diller_cars(id: int, db: Session = Depends(get_db)):
    db_diller = crud.get_diller(db, id)
    if db_diller is None:
        raise HTTPException(status_code=404, detail="Diller with this id not found")
    dillers_cars = crud.get_dillers_cars(db, id)
    return dillers_cars


@app.delete("/cars/{id}")
def delete_cars(id: int, db: Session = Depends(get_db)):
    car_id = crud.get_car(db, id)
    if car_id is None:
        raise HTTPException(status_code=404, detail="Car with this id not found")
    return crud.delete_car(db, id)


@app.delete("/dillers/{id}")
def delete_dillers(id: int, db: Session = Depends(get_db)):
    diller_id = crud.get_diller(db, id)
    if diller_id is None:
        raise HTTPException(status_code=404, detail="Diller with this id not found")
    return crud.delete_diller(db, id)


@app.put("/cars/{id}")
def update_car(car: schemas.CarsSchema, id: int, db: Session = Depends(get_db)):
    db_car = crud.get_car(db, id)
    if db_car is None:
        raise HTTPException(status_code=404, detail="Car with this id not found")
    return crud.update_car(db_car=db_car, db=db, car=car)


@app.put("/dillers/{id}")
def updata_dillers(diller: schemas.DillerSchema, id: int, db: Session = Depends(get_db)):
    db_diller = crud.get_diller(db, id)
    if db_diller is None:
        raise HTTPException(status_code=404, detail="Diller with this id not found")
    return crud.update_diller(db_diller=db_diller, db=db, diller=diller)
