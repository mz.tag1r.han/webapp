from sqlalchemy.orm import Session

import models, schemas


def get_car(db: Session, car_id: int):
    return db.query(models.Car).filter(models.Car.id == car_id).first()


def get_diller(db: Session, diller_id: int):
    return db.query(models.Diller).filter(models.Diller.id == diller_id).first()


def get_dillers_cars(db: Session, diller_id: int):
    return db.query(models.DillerCar).filter(models.DillerCar.diller_id == diller_id).all()


def get_all_dillers(db: Session):
    return db.query(models.Diller).all()


def get_all_cars_owners(db: Session):
    return db.query(models.DillerCar).all()


def delete_car(db: Session, car_id: int):
    db_car = db.query(models.Car).filter(models.Car.id == car_id).first()
    db.delete(db_car)
    db.commit()
    return db_car


def delete_diller(db: Session, diller_id: int):
    db_diller = db.query(models.Diller).filter(models.Diller.id == diller_id).first()
    db.delete(db_diller)
    db.commit()
    return db_diller


def create_car(db: Session, car: schemas.CarsSchema):
    db_car = models.Car(model=car.model, description=car.description)
    db.add(db_car)
    db.commit()
    db.refresh(db_car)
    return db_car


def create_car_own(db: Session, car_diller: schemas.DillerCarSchema):
    db_car_diller = models.DillerCar(diller_id=car_diller.diller_id, car_id=car_diller.car_id, amount=car_diller.amount)
    db.add(db_car_diller)
    db.commit()
    db.refresh(db_car_diller)
    return db_car_diller


def create_diller(db: Session, diller: schemas.DillerSchema):
    db_diller = models.Diller(name=diller.name, address=diller.address, phone_number=diller.phone_number)
    db.add(db_diller)
    db.commit()
    db.refresh(db_diller)
    return db_diller


def update_diller(db_diller, db: Session, diller: schemas.DillerSchema):
    diller_data = diller.dict(exclude_unset=True)
    for key, value in diller_data.items():
        setattr(db_diller, key, value)
    db.add(db_diller)
    db.commit()
    db.refresh(db_diller)
    return diller


def update_car(db_car, db: Session, car: schemas.CarsSchema):
    car_data = car.dict(exclude_unset=True)
    for key, value in car_data.items():
        setattr(db_car, key, value)
    db.add(db_car)
    db.commit()
    db.refresh(db_car)
    return car
