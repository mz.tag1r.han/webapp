from sqlalchemy import Column, String, Integer, BIGINT, DateTime, ForeignKey
from sqlalchemy.orm import relationship, backref
from database import Base


class Diller(Base):
    __tablename__ = "dillers"

    id = Column(Integer, primary_key=True, index=True, unique=True)
    name = Column(String)
    address = Column(String)
    phone_number = Column(BIGINT)


class Car(Base):
    __tablename__ = "cars"

    id = Column(Integer, primary_key=True, index=True, unique=True)
    model = Column(String)
    description = Column(String)


class DillerCar(Base):
    __tablename__ = "dillerCar"

    id = Column(Integer, primary_key=True, index=True, unique=True)
    diller_id = Column(Integer, ForeignKey("dillers.id", ondelete='cascade'))
    car_id = Column(Integer, ForeignKey("cars.id", ondelete='cascade'))
    amount = Column(Integer)
